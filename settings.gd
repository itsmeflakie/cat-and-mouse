extends Node

const BLOCK_SIZE := 32

var dedicated := false # server can host without a player inside
var client := true

func _ready():
	# make background color black
	for s in OS.get_cmdline_args():
		if s == "-serve": dedicated = true
	VisualServer.set_default_clear_color(Color(0, 0, 0))