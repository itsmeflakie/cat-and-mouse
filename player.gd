extends KinematicBody2D

const ZEROVEC = Vector2(0, 0)

const MAX_MOVE_SPEED := 200
const ACCELARATION := 400
const FRICTION := 4
const FADE_SPEED := 4

const RUN_ANIMATION_MAX_SPEED := 0.9
const RUN_ANIMATION_MIN_SPEED := 0.5

puppet var puppet_pos := Vector2(0, 0)
puppet var move := Vector2(0, 0)
puppet var moving := false
puppet var scale_x := 1

var moveDir := Vector2(0, 0)

var hunter # if this is a thing, then we're gonna fade out when it can't see us
var seen := true

func move(dir: Vector2) -> void:
	moveDir = dir.normalized()
	moving = true
	rset("moving", true)

func stop() -> void:
	moving = false
	rset("moving", false)

func set_camera_focus() -> void: $camera.current = true
func unset_camera_focus() -> void: $camera.current = false

func _ready():
	$animation.play()

func _physics_process(delta):
	if is_network_master():
		if move.length() > 0:
			rset_unreliable("move", move)
			rset_unreliable("puppet_pos", position)
		
		if moving:
			move += moveDir * delta * ACCELARATION
			move = move.clamped(MAX_MOVE_SPEED)
			if moveDir.x > 0:
				scale_x = 1
			else:
				scale_x = -1
			rset("scale_x", scale_x)
		else:
			move = move.linear_interpolate(Vector2(0, 0), FRICTION * delta)
	else:
		position = puppet_pos
		
		# fade from hunter's sight
		if hunter != null:
			$visibilityRay.enabled = true
			$visibilityRay.cast_to = to_local(hunter.position)
			
			seen = not $visibilityRay.is_colliding()
		else:
			$visibilityRay.enabled = false
			seen = true
	
	move = move_and_slide(move)
	
	# appearance
	$sprite.scale.x = scale_x
	if moving:
		$animation.current_animation = "mouseRun"
		$animation.playback_speed = max(RUN_ANIMATION_MIN_SPEED, min(RUN_ANIMATION_MAX_SPEED, 1 - (max(abs(move.x), abs(move.y)) / MAX_MOVE_SPEED) * RUN_ANIMATION_MIN_SPEED))
	else:
		$animation.current_animation = "mouseIdle"
	
	if seen:
		$sprite.modulate.a = min(1, $sprite.modulate.a + delta * FADE_SPEED)
	else:
		$sprite.modulate.a = max(0, $sprite.modulate.a - delta * FADE_SPEED)