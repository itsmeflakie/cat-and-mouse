extends MeshInstance2D

export (int) var width = 10
export (int) var height = 10

func _ready():
	var st = SurfaceTool.new()
	
	st.begin(Mesh.PRIMITIVE_TRIANGLE_FAN)
	
	var endX = settings.BLOCK_SIZE * width
	var endY = settings.BLOCK_SIZE * height
	
	var uvX = endX / texture.get_width()
	var uvY = endY / texture.get_height()
	
	st.add_uv(Vector2(0, 0))
	st.add_vertex(Vector3(0, 0, 0))
	st.add_uv(Vector2(0, uvY))
	st.add_vertex(Vector3(0, endY, 0))
	st.add_uv(Vector2(uvX, uvY))
	st.add_vertex(Vector3(endX, endY, 0))
	st.add_uv(Vector2(uvX, 0))
	st.add_vertex(Vector3(endX, 0, 0))
	
	mesh = st.commit()