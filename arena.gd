extends Node2D

var player
var player_peer_ids = {}

func focus_on_player(player: KinematicBody2D) -> void:
	for p in $players.get_children():
		p.unset_camera_focus()
		p.hunter = player
	player.set_camera_focus()
	player.hunter = null

remote func register(new_peer_id: int, is_player: bool = false) -> void:
	if player_peer_ids.has(new_peer_id): return # ensure no overwriting other players
	player_peer_ids[new_peer_id] = true  # index peer
	
	# instance the player node here
	var new_player = preload("res://player.tscn").instance()
	new_player.name = str(new_peer_id)
	new_player.position = $mouseSpawn.position
	new_player.hunter = player
	new_player.set_network_master(new_peer_id)
	$players.add_child(new_player)
	
	# if this is our player, give it focus and control
	if is_player:
		player = new_player
		focus_on_player(new_player)
	
	# register all players for the new player, from the server
	if get_tree().is_network_server():
		if not settings.dedicated: rpc_id(new_peer_id, "register", 1) # register server player, unless dedicated
		
		for peer_id in player_peer_ids:
			rpc_id(new_peer_id, "register", peer_id, true)

func _ready():
	if not settings.dedicated: register(1, true)
	
	$light.visible = true
	$CanvasModulate.visible = true
	$occluders.visible = true
	
	get_tree().connect("connected_to_server", self, "_connected")
	get_tree().connect("network_peer_connected", self, "_peer_connect")

func _peer_connect():
	print("CONNECCTTTTTTEDDDDD")

func _connected():
	rpc("register", get_tree().get_network_unique_id())

func _physics_process(delta):
	if player:
		$light.position = player.position

func _unhandled_input(event):
	if event is InputEventScreenTouch or event is InputEventScreenDrag:
		player.move((event.position - get_viewport_rect().end/2))
	
	if not event.is_pressed() and event is InputEventScreenTouch:
		player.stop()