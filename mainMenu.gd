extends Control

var server_port = 4444
var server_ip = "35.209.75.55"
const MAX_PLAYERS := 500

func _ready():
	if settings.dedicated:
		_on_server_pressed()

func _on_client_pressed():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, server_port)
	get_tree().set_network_peer(peer)
	get_tree().change_scene("res://arena.tscn")

func _on_server_pressed():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(server_port, MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	get_tree().change_scene("res://arena.tscn")
